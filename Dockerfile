FROM python:3.8

RUN apt-get update
RUN apt-get install git

RUN mkdir /app
WORKDIR /app

COPY ./ ./

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["bash"]
